/*
 * Author: Stefan Lustenberger
 * Date: 2023-05-12
 */

const canvas = document.querySelector("#pong");
const ctx = canvas.getContext("2d");

const canWidth = canvas.width;
const canHeight = canvas.height;
const RADIUS = 5;
const paddleHeight = 100;
const paddleWidth = 10;
const paddleOffset = 10;
const paddleZone1Border = 0.05; // when hit at the outer edges, y-velocity changes dramatically
const paddleZone2Border = 0.2; // when hit a little away from the edges , y-velocity changes a little
const yVelocityChange = 0.2; // the amount the y-velocity is changed when hitting the inner paddle border zone
const zone1Factor = 3; // factor for yVelocityChange in outer border Zone
const maxYVelocity = 4;
const paddleX1 = paddleOffset;
const paddleX2 = canWidth - paddleWidth - paddleOffset;
const winningCondition = 5;
const player1Name = prompt("Please enter the name of Player 1");
const player2Name = prompt("Please enter the name of Player 2")

let ballX = canWidth / 2; // initial x
let ballY = canHeight / 2; // initial y
let xVelocity = 2;
let xDirection = initialDirection(); // initial x-velocity
let yVelocity = 2 * initialDirection(); // initial y-velocity
let paddleY1 = (canHeight - paddleHeight) / 2;
let paddleY2 = (canHeight - paddleHeight) / 2;
let score1 = 0;
let score2 = 0;

function renderBackground() {
  ctx.fillStyle = "black";
  ctx.fillRect(0, 0, canWidth, canHeight);

  // the net
  ctx.strokeStyle = "white";
  ctx.beginPath();
  ctx.setLineDash([5, 15]);
  ctx.moveTo(canWidth / 2, 0);
  ctx.lineTo(canWidth / 2, canHeight);
  ctx.stroke();

  // the scores
  ctx.font = "2rem serif";
  ctx.fillStyle = "lightgray";
  ctx.fillText(player1Name, 20, 40);
  ctx.fillText(player2Name, canWidth - 130, 40);
  ctx.fillText("Best of " + (winningCondition * 2 - 1), 285, 40);
  if (score1 > score2) ctx.fillStyle = "yellow";
  if (score1 < score2) ctx.fillStyle = "orange";
  ctx.fillText(score1, 20, 80);
  if (score1 > score2) ctx.fillStyle = "orange";
  if (score1 < score2) ctx.fillStyle = "yellow";
  ctx.fillText(score2, canWidth - 50, 80);
 
}

function renderBall(x, y) {
  ctx.beginPath();
  ctx.arc(x, y, RADIUS, 0, 2 * Math.PI);
  ctx.fillStyle = "red";
  ctx.fill();
}

function renderPaddle(x, y) {
  ctx.fillStyle = "gray";
  ctx.fillRect(x, y, paddleWidth, paddleHeight);
  ctx.fillStyle = "silver";
  ctx.fillRect(
    x,
    y + paddleHeight * paddleZone1Border,
    paddleWidth,
    paddleHeight * (1 - 2 * paddleZone1Border)
  );
  ctx.fillStyle = "white";
  ctx.fillRect(
    x,
    y + paddleHeight * paddleZone2Border,
    paddleWidth,
    paddleHeight * (1 - 2 * paddleZone2Border)
  );
}

function checkPaddleCollision() {
  return (
    (ballX - RADIUS <= paddleX1 + paddleWidth &&
      ballX - RADIUS > paddleX1 + paddleWidth - xVelocity &&
      ballY >= paddleY1 &&
      ballY <= paddleY1 + paddleHeight &&
      xDirection < 0) ||
    (ballX + RADIUS >= paddleX2 &&
      ballX + RADIUS < paddleX2 + xVelocity &&
      ballY >= paddleY2 &&
      ballY <= paddleY2 + paddleHeight &&
      xDirection > 0)
  );
}

function yChange() {
  if (
    (xDirection < 0 && ballY <= paddleY1 + paddleHeight * paddleZone1Border) ||
    (xDirection > 0 && ballY <= paddleY2 + paddleHeight * paddleZone1Border)
  ) {
    yVelocity -= yVelocityChange * zone1Factor;
  } else if (
    (xDirection < 0 && ballY <= paddleY1 + paddleHeight * paddleZone2Border) ||
    (xDirection > 0 && ballY <= paddleY2 + paddleHeight * paddleZone2Border)
  ) {
    yVelocity -= yVelocityChange;
  }
  if (
    (xDirection < 0 &&
      ballY >= paddleY1 + paddleHeight - paddleHeight * paddleZone1Border) ||
    (xDirection > 0 &&
      ballY >= paddleY2 + paddleHeight - paddleHeight * paddleZone1Border)
  ) {
    yVelocity += yVelocityChange * zone1Factor;
  } else if (
    (xDirection < 0 &&
      ballY >= paddleY1 + paddleHeight - paddleHeight * paddleZone2Border) ||
    (xDirection > 0 &&
      ballY >= paddleY2 + paddleHeight - paddleHeight * paddleZone2Border)
  ) {
    yVelocity += yVelocityChange;
  }
  if (yVelocity > maxYVelocity) {
    yVelocity = maxYVelocity;
  }
  if (yVelocity < -maxYVelocity) {
    yVelocity = -maxYVelocity;
  }
}

function initialDirection() {
  if (Math.random() < 0.5) return -1;
  return 1;
}

let intervalID = setInterval(main, 16);

function main() {
  renderBackground();
  renderBall(ballX, ballY);
  renderPaddle(paddleX1, paddleY1);
  renderPaddle(paddleX2, paddleY2);

  // we have a WINNER!!!
  if (score1 === winningCondition) {
    clearInterval(intervalID);
    ctx.fillStyle = "yellow";
    ctx.font = "4rem serif";
    ctx.fillText(`${player1Name} has won`, 150, canHeight / 2);
  }
  if (score2 === winningCondition) {
    clearInterval(intervalID);
    ctx.fillStyle = "yellow";
    ctx.font = "4rem serif";
    ctx.fillText(`${player2Name} has won`, 150, canHeight / 2);
  }

  // Wall collisions
  if (ballX - RADIUS <= 0) {
    xDirection *= -1;
    score2 += 1;
    ctx.fillStyle = "red";
    ctx.fillRect(0, 0, canWidth / 8, canHeight);
  }
  if (ballX + RADIUS >= canWidth) {
    xDirection *= -1;
    score1 += 1;
    ctx.fillStyle = "red";
    ctx.fillRect((canWidth * 7) / 8, 0, canWidth / 8, canHeight);
  }
  if (ballY + RADIUS >= canHeight || ballY - RADIUS <= 0) {
    yVelocity *= -1;
  }

  // paddle collisions
  if (checkPaddleCollision()) {
    yChange();
    xDirection *= -1;
  }

  // ball moves
  ballX += xDirection * xVelocity;
  ballY += yVelocity;
}

document.addEventListener("keydown", (ev) => {
  if (paddleY1 > 0 && ev.code === "KeyW") {
    // move up
    paddleY1 -= 5;
  }
  if (paddleY1 + paddleHeight < canHeight && ev.code === "KeyS") {
    // move down
    paddleY1 += 5;
  }
  if (paddleY2 > 0 && ev.code === "ArrowUp") {
    // move up
    paddleY2 -= 5;
  }
  if (paddleY2 + paddleHeight < canHeight && ev.code === "ArrowDown") {
    // move down
    paddleY2 += 5;
  }
});
